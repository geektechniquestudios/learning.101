package com.geektechnique.norest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoRestApplication.class, args);
    }

}
