package com.geektechnique.evolutionrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvolutionRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvolutionRestApplication.class, args);
    }

}
