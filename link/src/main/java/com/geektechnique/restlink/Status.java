package com.geektechnique.restlink;

enum Status {

	IN_PROGRESS,
	COMPLETED,
	CANCELLED;
}
