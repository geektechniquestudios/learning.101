package com.geektechnique.restlink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestLinkApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestLinkApplication.class, args);
    }

}
